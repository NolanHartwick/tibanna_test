### Snakemake / Tibanna Testing Grounds
This repo exists to house some code used to test snakemake/tibanna functionality. It also demonstrates at least one bug related to how "temp" and tibanna interact. Basically, this is a minimal snakemake workflow for working with tibanna

### Temp bug
When snakemake is run using tibanna, the temp function seems to be ignored. To demonstrate, run something like the following. (assuming you have git and snakemake and tibanna installed and setup)

```
git clone https://gitlab.com/NolanHartwick/tibanna_test.git
cd tibanna_test
snakemake --tibanna --default-remote-prefix=your-bucket/tibanna-test-dir --use-conda -p
```

This should result in tibanna executing, creating an s3 folder in 'your-bucket' named 'tibanna-test-dir'. This directory will have two files, "temp.txt" and "test.txt", the former of which should probably be deleted as a result of being marked as a temporary file in the snakemake workflow


