import multiprocessing
import os
from psutil import virtual_memory
from os.path import expanduser


home = expanduser("~")
pypath = os.path.dirname(os.path.realpath(__file__))

output_path = snakemake.output[0]
temp_output = snakemake.output[1]
dummy_path = "accidental-file.txt"


with open(output_path, "w") as fout:
    fout.write(f"CWD: {os.getcwd()}\n")
    fout.write(f"Home: {home}\n")
    fout.write(f"pypath: {pypath}\n")
    fout.write(f"CPUs : {multiprocessing.cpu_count()}\n")
    fout.write(f"Disk Size CWD: {os.statvfs('.')}\n")
    fout.write(f"Disk Size Home: {os.statvfs(home)}\n")
    fout.write(f"Disk Size pypath: {os.statvfs(pypath)}\n")
    fout.write(f"Mem Size : {virtual_memory().total}\n")

with open(dummy_path, "w") as fout:
    fout.write("hopefully, this file doesn't inserted into s3 or downloaded.")

with open(temp_output, "w") as fout:
    fout.write("Hopefully, this gile gets inserted and deleted")

