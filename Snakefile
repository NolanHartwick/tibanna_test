


rule all:
    input:
        "test.txt"
    group:
        "all"


rule maketest:
    output:
        "test.txt",
        temp("temp.txt")
    threads:
        32
    resources:
        mem_mb=120*1024,
        disk_mb=2*1024*1024
    conda:
        "requirements.yml"
    group:
        "all"
    script:
        "get_info.py"


